package trigonomietria.basica;
/**
 * @author seba traverso
 * @version 1.1
 */

public class Triangulo {
        /**
        * Variable privada: Nombre del autor
        */
	private Punto p1;
	private Punto p2;
	private Punto p3;
	
	
	/**
        * Constructor sin parameros     
        */
	public Triangulo(){
		p1 = new Punto(0,0);
		p2 = new Punto(5,0);
		p3 = new Punto(0,5);
	}
	
        /**
        * Constructor con parameros     
        * @param paramP1 tipo Punto
        * @param paramP2 tipo Punto
        * @param paramP3 tipo Punto
        */
	public Triangulo(Punto paramP1, Punto paramP2, Punto paramP3){
		p1 = new Punto(paramP1.getX(), paramP1.getY());
		p2 = new Punto(paramP2.getX(), paramP2.getY());
		p3 = new Punto(paramP3.getX(), paramP3.getY());
	}
	
        /**
        * Constructor con parameros     
        * @param x1 tipo double
        * @param x2 tipo double
        * @param x3 tipo double
        * @param y1 tipo double
        * @param y2 tipo double
        * @param y3 tipo double
        */
	public Triangulo(double x1, double y1, double x2, double y2, double x3, double y3){
		p1 = new Punto(x1, y1);
		p2 = new Punto(x2, y2);
		p3 = new Punto(x3, y3);
	}
	
        /**
        * Devuelve el punto 1
        * @return Punto
        */
	public Punto getP1() {
		return p1;
	}
	
        /**
        * Setea el punto 1
        * @param p1 tipo Punto
        */
        public void setP1(Punto p1) {
		this.p1 = p1;
	}
	
        /**
        * Devuelve el punto 2
        * @return Punto
        */
        public Punto getP2() {
		return p2;
	}
	
        /**
        * Setea el punto 2
        * @param p2 tipo Punto
        */
        public void setP2(Punto p2) {
		this.p2 = p2;
	}
	
        /**
        * Devuelve el punto 3
        * @return Punto
        */
        public Punto getP3() {
		return p3;
	}
	
        /**
        * Setea el punto 2
        * @param p3  tipo Punto
        */
        public void setP3(Punto p3) {
		this.p3 = p3;
	}
		
	/**
        * Metodos que calcula la distancia mia a un Punto
        * @param desde tipo Punto
        * @return valor double
        */
	public double calcularDistanciaDesde(Punto desde){
		double valorX;
		double valorY;
		
		valorX = Math.pow((p1.getX() - desde.getX()), 2);
		valorY = Math.pow((p1.getY() - desde.getX()), 2);
		
		return Math.sqrt(valorX + valorY);
	}
	
        /**
        * Metodos que calcula el area
        * @return valor double
        */
	public double calcularArea(){
		double izqDer = 0;
		double derIzq = 0;
		double areaTotal;
		
		izqDer = (this.getP1().getX() * this.getP2().getY()) + 
				 (this.getP2().getX() * this.getP3().getY()) +
				 (this.getP3().getX() * this.getP1().getX());
		
		derIzq = (this.getP1().getY() * this.getP2().getX()) +
				 (this.getP2().getY() * this.getP3().getX()) +
				 (this.getP3().getY() * this.getP1().getX());
		
		areaTotal = izqDer - derIzq;
		
		if (areaTotal < 0)
			areaTotal = areaTotal * (-1);
		
		return areaTotal;
	}
	
        /**
        * Metodos que calcula el perimetro
        * @return valor double
        */
	public double calcularPerimetro(){
		double a = 0;
		
		return a;
	}

}
