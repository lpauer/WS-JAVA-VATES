/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trigonomietria.basica;

/**
 *
 * @author anonimo
 */
public class Cuadrado {
    
    Punto p1;
    Punto p2;
    Punto p3;
    Punto p4;
    
    public Cuadrado(){
        p1 = new Punto(0,0);
        p2 = new Punto(5,0);
        p3 = new Punto(0,5);
        p4 = new Punto(5,5);
    }
    
    public Cuadrado(Punto pto1, Punto pto2, Punto pto3, Punto pto4){
        p1 = pto1;
        p2 = pto2;
        p3 = pto3;
        p4 = pto4;
    }

    public Punto getP1() {
        return p1;
    }

    public void setP1(Punto p1) {
        this.p1 = p1;
    }

    public Punto getP2() {
        return p2;
    }

    public void setP2(Punto p2) {
        this.p2 = p2;
    }

    public Punto getP3() {
        return p3;
    }

    public void setP3(Punto p3) {
        this.p3 = p3;
    }

    public Punto getP4() {
        return p4;
    }

    public void setP4(Punto p4) {
        this.p4 = p4;
    }
    
    
    
    
}
