/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vectores;

import java.util.Scanner;

/**
 *
 * @author anonimo
 */
public class Vectores {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int [] vecNumeros = {11, -22, 33, -44, 55, -66, 77, -88, 99};
        int [] vecPositivos = new int[10];
        int [] vecNegativos = new int[10];
        
        int posPos = 0;
        int posNeg = 0;
        int totalPositivos = 0;
        int totalNegativos = 0;
        
        Scanner input = new Scanner(System.in);
        int primer = 0;
        int segundo = 0;
        int tercer = 0;
        
        for(int i = 0; i < vecNumeros.length; i++){
            if(vecNumeros[i] >=0){
                vecPositivos[posPos] = vecNumeros[i];
                posPos++;
                totalPositivos += vecNumeros[i];
            }
            else {
                vecNegativos[posNeg] = vecNumeros[i];
                posNeg++;
                totalNegativos += vecNumeros[i];
            }
        }
        
        System.out.println("Vector Numeros");
        imprimirVector(vecNumeros);
        
        System.out.println("Vector Positivos");
        imprimirVector(vecPositivos);
        
        System.out.println("Vector Negativos");
        imprimirVector(vecNegativos);
        
        
        System.out.println("Total de Posistivos: " + totalPositivos);
        System.out.println("Total de Negativos: " + totalNegativos);
        
        
        System.out.println("Ingrese primer valor ");
        primer = input.nextInt();
        
        System.out.println("Ingrese segundo valor ");
        segundo = input.nextInt();
        
        System.out.println("Ingrese tercer valor ");
        tercer = input.nextInt();
        
        System.out.println("Primer valor " + primer);
        System.out.println("Segundo valor " + segundo);
        System.out.println("Tercer valor " + tercer);
        
    }
    
    public static void imprimirVector(int[] algo){
        System.out.print("[");
        
        for(int i = 0; i < algo.length; i++){
            System.out.print(algo[i] + ",");
        }
        
        System.out.println("]");
    }

    
}
