/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7;

/**
 *
 * @author anonimo
 */
public class Numero {
    
    public static int  valor_estatico = 3;   
    
    private int otro_valor;
    
    
    public void set_otro_valor (int valor){
        this.otro_valor = valor;
    }
    
    public int get_otro_valor(){
        return this.otro_valor;
    }
    
    @Override
    public String toString(){
        return "Numero: " + this.get_otro_valor();
    }
    
    
    public static int getValor_estatico() {
        return valor_estatico;
    }

    public static void setValor_estatico(int valor_estatico) {
        Numero.valor_estatico = valor_estatico;
    }
    
    
}
