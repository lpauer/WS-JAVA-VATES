package com.vates;

import java.util.Scanner;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        boolean ejecutarTodo = true;

        if (ejecutarTodo) {

            ejercicio2();


            ejercicio3();


            ejercicio4();


            ejercicio5();


            ejercicio6(12);

            ejercicio7();


            ejercicio8();

            ejercicio9(5);


            ejercicio10();


            ejercicio11();

            ejercicio12();


            ejercicio13();

            ejercicio14();


            ejercicio15();

            ejercicio16();

            ejercicio17();

            ejercicio18();


            ejercicio19();
        }


        ejercicio20();

    }

    private static String cualEsMayor(int param1, int param2) {
        if (param1 == param2) {
            return "son iguales";
        }
        if (param1 > param2) {
            return "el primero es mayor";
        }
        if (param1 < param2) {
            return "el segundo es mayor";
        }
        return null;
    }

    private static void ejercicio2() {

        System.out.println("2. --------------------");
        System.out.println("Hola Mundo");
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio3() {

        System.out.println("3. --------------------");
        int ej3_nro1 = 7;
        int ej3_nro2 = 5;
        System.out.println("Nro1: " + ej3_nro1);
        System.out.println("Nro2: " + ej3_nro2);
        System.out.println("Suma: " + (ej3_nro1 + ej3_nro2));
        System.out.println("Resta: " + (ej3_nro1 - ej3_nro2));
        System.out.println("Multiplicacion: " + (ej3_nro1 * ej3_nro2));
        System.out.println("Division: " + (ej3_nro1 / ej3_nro2));
        System.out.println("Modulo: " + (ej3_nro1 % ej3_nro2));
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio4() {

        System.out.println("4. --------------------");
        int ej4_nro1 = 6;
        int ej4_nro2 = 3;
        System.out.println("Nro1: " + ej4_nro1);
        System.out.println("Nro2: " + ej4_nro2);
        System.out.println("El mayor es: " + cualEsMayor(ej4_nro1, ej4_nro2));
        ej4_nro1 = 6;
        ej4_nro2 = 9;
        System.out.println("Nro1: " + ej4_nro1);
        System.out.println("Nro2: " + ej4_nro2);
        System.out.println("El mayor es: " + cualEsMayor(ej4_nro1, ej4_nro2));
        ej4_nro1 = 6;
        ej4_nro2 = 6;
        System.out.println("Nro1: " + ej4_nro1);
        System.out.println("Nro2: " + ej4_nro2);
        System.out.println("El mayor es: " + cualEsMayor(ej4_nro1, ej4_nro2));
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio5() {

        System.out.println("5. --------------------");
        int nro_primos = 1000;
        int bisiest_desde = 2000;
        int bisiest_hasta = 3000;

        primos(nro_primos);
        System.out.println("");

        bisiestos(bisiest_desde, bisiest_hasta);
    }

    private static void bisiestos(int desde, int hasta) {
        System.out.println("Bisiestos --------------------");
        for (int j = desde; j <= hasta; j++) {
            if (j % 4 == 0) {
                System.out.println("Nro: " + j);
            }
        }
        System.out.println("");
        System.out.println("");
    }

    private static void primos(int param) {
        System.out.println("Primos --------------------");
        int v_cant_submultiplos;

        for (int i = 1; i <= param; i++) {
            v_cant_submultiplos = 0;

            //contar cant veces divisible
            for (int y = 1; y <= i; y++) {
                if (i % y == 0) {
                    v_cant_submultiplos++;
                }
            }

            if (v_cant_submultiplos++ <= 2) {
                System.out.println("Nro. " + i);
            }
        }
    }

    private static void ejercicio6(int param) {
        System.out.println("6. --------------------");
        int v_factorial = 1;
        System.out.println("El factorial de " + param);

        if (param == 0) {
            System.out.println("Nro: 1");
        } else {
            for (int i = 1; i < param; i++) {
                v_factorial = v_factorial * i;
            }
            System.out.println("Nro: " + v_factorial);
        }
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio7() {

        System.out.println("7. --------------------");
        int v_sumaTotal = 0;

        for (int i = 0; i <= 100; i++) {
            v_sumaTotal = v_sumaTotal + i;
        }
        System.out.println("La suma de 1 a 100 es: " + v_sumaTotal);
        System.out.println("");
        System.out.println("");

    }

    private static void ejercicio8() {

        System.out.println("8. --------------------");

        int valores[] = new int[5];

        valores[0] = 7;
        valores[1] = 1;
        valores[2] = 8;
        valores[3] = 3;
        valores[4] = 9;

        System.out.println("Valores del vector:");

        for (int i = 0; i < valores.length; i++) {
            System.out.println("Valor vector[" + i + "]:" + valores[i]);
        }
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio9(int longitud) {

        System.out.println("9. --------------------");
        int valores[] = new int[longitud];
        Scanner input = new Scanner(System.in);

        for (int j = 0; j < longitud; j++) {
            System.out.println("Ingrese posicion " + j);
            //input.next();
            valores[j] = input.nextInt();
        }

        System.out.println("Valores del vector:");

        for (int i = 0; i < valores.length; i++) {
            System.out.println("Valor vector[" + i + "]:" + valores[i]);
        }


        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio10() {

        System.out.println("10. --------------------");

        Scanner input2 = new Scanner(System.in);
        int longit;
        int valores[];

        System.out.println("Ingrese la longitud: ");
        longit = input2.nextInt();
        valores = new int[longit];

        completarArray(valores);

        imprimirArray(valores);

        //input2.close();
        //input2 = null;

        System.out.println("");
        System.out.println("");
    }

    private static void completarArray(int[] valores) {
        Scanner input = new Scanner(System.in);
        int multiplo;

        System.out.println("Ingrese el multiplo: ");
        multiplo = input.nextInt();

        for (int i = 0; i < valores.length; i++) {
            valores[i] = (i + 1) * multiplo;
        }


    }

    private static void imprimirArray(int[] valores) {
        System.out.println("Valores del vector:");

        for (int i = 0; i < valores.length; i++) {
            System.out.println("Valor vector[" + i + "]:" + valores[i]);
        }
    }


    private static void ejercicio11() {

        System.out.println("11. --------------------");

        Scanner input = new Scanner(System.in);
        int valores[] = new int[10];
        int mediaPares;

        for (int j = 0; j < valores.length; j++) {
            System.out.println("Ingrese posicion " + j);
            valores[j] = input.nextInt();
        }

        mediaPares = calcularPares(valores);
        System.out.println("La media es: " + mediaPares);
    }

    private static int calcularPares(int[] valores) {
        int media = 0;

        for (int i = 0; i < valores.length; i++) {
            if ((i + 1) % 2 == 0) {
                media += valores[i];
            }
        }

        media = media / (valores.length / 2);

        return media;
    }

    private static void ejercicio12() {

        System.out.println("12. --------------------");

        Scanner input = new Scanner(System.in);
        int valores[] = new int[10];
        int mediaPares = 0;
        int mediaImpares = 0;
        int cantPares = 0;
        int cantImpares = 0;

        for (int j = 0; j < valores.length; j++) {
            System.out.println("Ingrese posicion " + j);
            valores[j] = input.nextInt();
        }

        for (int i = 0; i < valores.length; i++) {
            if (valores[i] >= 0) {
                mediaPares = mediaPares + valores[i];
                cantPares++;
            } else {
                mediaImpares = mediaImpares + valores[i];
                cantImpares++;
            }
        }

        if (cantPares > 0)
            System.out.println("Media Pares: " + (mediaPares / cantPares));
        else
            System.out.println("No hay valores Pares");

        if (cantImpares > 0)
            System.out.println("Media Impares: " + (mediaImpares / cantImpares));
        else
            System.out.println("No hay valores Impares");

        System.out.println("");
        System.out.println("");
    }


    private static void ejercicio13() {

        System.out.println("13. --------------------");

        Scanner input = new Scanner(System.in);
        int valores = 0;

        System.out.println("Ingrese un nro: ");
        valores = input.nextInt();

        if (valores % 2 == 0)
            System.out.println("Es divisible por 2");
        else
            System.out.println("NO es divisible por 2");

        System.out.println("");
        System.out.println("");
    }


    private static void ejercicio14() {

        System.out.println("14. --------------------");

        Scanner input = new Scanner(System.in);
        int valor = 0;
        char convertir;

        System.out.println("Ingrese un nro para convertir en ASCII: ");
        valor = input.nextInt();

        convertir = (char) valor;
        System.out.println("El caracter en ASCII es: " + convertir);

        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio15() {

        System.out.println("15. --------------------");

        Scanner input = new Scanner(System.in);
        double precio = 0;
        final double iva = 1.21;
        double total;

        System.out.println("Ingrese el precio SIN IVA (con coma, NO punto): ");
        precio = input.nextDouble();

        total = precio * iva;
        System.out.println("Precio CON IVA: " + total);

        System.out.println("");
        System.out.println("");
    }


    private static void ejercicio16() {

        System.out.println("16. --------------------");

        Scanner input = new Scanner(System.in);
        int cantVentas = 0;
        double monto = 0;
        double total = 0;

        System.out.println("Ingrese la cantidad de ventas: ");
        cantVentas = input.nextInt();

        for (int i = 0; i < cantVentas; i++) {
            System.out.println("Ingrese el monto de la venta " + (i + 1) + " (usar coma): ");
            monto = input.nextDouble();
            total += monto;
        }

        System.out.println("El total vendido es: " + total);


        System.out.println("");
        System.out.println("");
    }


    private static void ejercicio17() {

        System.out.println("17. --------------------");

        Scanner input = new Scanner(System.in);
        double variableA = 0.0;
        double variableB = 0.0;
        double variableC = 0.0;
        double resultado1 = 0.0;
        double resultado2 = 0.0;

        do {
            System.out.println("Ingrese la variable A (distinto de cero): ");
            variableA = input.nextDouble();
        } while (variableA == 0);

        System.out.println("Ingrese la variable B: ");
        variableB = input.nextDouble();
        System.out.println("Ingrese la variable C: ");
        variableC = input.nextDouble();

        System.out.println("valor de A: " + variableA);
        System.out.println("valor de B: " + variableB);
        System.out.println("valor de C: " + variableC);

        resultado1 = ((-variableB) + Math.sqrt(Math.pow(variableB, 2) - (4 * variableA * variableC))) / (2 * variableA);
        resultado2 = ((-variableB) - Math.sqrt(Math.pow(variableB, 2) - (4 * variableA * variableC))) / (2 * variableA);

        System.out.println("El primer resultado es: " + resultado1);   //VER devuelve NaN
        System.out.println("El segundo resultado es: " + resultado2);  //VER devuelve NaN

        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio18() {

        System.out.println("18. --------------------");

        Scanner input = new Scanner(System.in);
        int variableA = 0;

        do {
            System.out.println("Ingrese un valor (mayor o igual a cero): ");
            variableA = input.nextInt();
        } while (variableA < 0);

        System.out.println("El valor ingresado es: " + variableA);


        System.out.println("");

    }

    private static void ejercicio19() {

        System.out.println("19. --------------------");

        Scanner input = new Scanner(System.in);
        String contraseña = "sebas";
        String ingreso;
        int intentos = 0;
        boolean correcto = false;

        do {
            System.out.println("Ingrese la contraseña: ");
            ingreso = input.nextLine();

            if (ingreso.compareTo(contraseña) == 0)
                correcto = true;
            else
                intentos++;

        } while ((!correcto) && intentos < 3);

        if (correcto)
            System.out.println("La contraseña es correcta");
        else
            System.out.println("La contraseña es incorrecta");

        System.out.println("");
    }

    private static void ejercicio20() {

        System.out.println("20. --------------------");

        Scanner input = new Scanner(System.in);
        int primero = 0;
        int segundo = 0;
        int aleatorio = 0;

        System.out.println("Ingrese el primer valor: ");
        primero = input.nextInt();

        System.out.println("Ingrese el segundo valor: ");
        segundo = input.nextInt();

        for (int i = 0; i < 10; i++) {
            aleatorio = (int) Math.floor(Math.random() * (segundo - primero + 1) + primero);
            System.out.println("random " + (i + 1) + ": " + aleatorio);
        }


        System.out.println("");

    }


}






